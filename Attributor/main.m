//
//  main.m
//  Attributor
//
//  Created by Mckein on 12/2/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AttributorAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AttributorAppDelegate class]));
    }
}
