//
//  AttributorAppDelegate.h
//  Attributor
//
//  Created by Mckein on 12/2/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AttributorAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
